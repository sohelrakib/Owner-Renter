<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Advcomment extends Model
{
    protected $table="advcomment";

    protected $fillable=[
            'advertise_id',
            'user_id',
            'comment'
                        ];

    public function getUserEmail()
    {
        return Users::where('id',$this->user_id)->first()->email;
    }                    
}
