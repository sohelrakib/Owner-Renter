<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class advertise extends Model
{
    protected $table="advertise";

    protected $fillable=[
            'user_id',
            'house_area',
            'price',
            'room_size',
            'room_number',
            'address',
            'file_name',
            'file_size',
            'status'
                        ];

    public function getUserEmail()
    {
        return Users::where('id',$this->user_id)->first()->email;
    }

    public function getUserPhone()
    {
        return Users::where('id',$this->user_id)->first()->phone;
    }
}
