<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Advcomment;

class AdvertiseCommentController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        echo  session()->get('advertises_id');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         session()->put('asas',session()->get('advertises_id'));
         $alldata=Advcomment::where('advertise_id','LIKE',session()->get('advertises_id'))->paginate(4);


         if(session()->get('user_email')=='admin@co.com'){
                    return view('admin.admin-advcomment',compact('alldata'));
                                                         }
         else{
                      if(session()->get('user_type')=='owner'){
                            
                                                              }
                      else{
                            
                          }                                       

              }

        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

       

            $this->validate($request,[
            'comment'=>'required',
         ]);

          $input['advertise_id']= session()->get('advertises_id');
          $input['user_id']= session()->get('user_id');
          $input['comment']=$request->input('comment');
        
        
           Advcomment::create($input);
           $message="you comment on it!";
           return redirect()->back()->with('message',$message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         session()->put('advertises_id',$id);

        return redirect('advcomment/create');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       echo "yyyyyesssssss commmmmmmminggggg.....";
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
