<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Advertise;
use App\Selection;

class AdvertiseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
       
         if(session()->get('user_email')=='admin@co.com'){
                    return view('admin.admin-advertise');
                                                         }
         else{
                      if(session()->get('user_type')=='owner'){
                            
                               return view('owner.owner-advertise');                                }
                                                           

              }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
             // my advertisement

    $alldata=Advertise::where('user_id','LIKE',session()->get('user_id'))->where('status','=',1)->paginate(2);

        if(session()->get('user_email')=='admin@co.com'){
                      return view('admin.admin-my-advertise',compact('alldata'));
                                                         }
        elseif(session()->get('user_type')=='owner'){
                                   
             echo "owner my advertise";             
                                                     }
                                                                  

                     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $this->validate($request,[
            'house_area'=>'required',
            'price'=>'required',
            'room_size'=>'required',
            'room_number'=>'required',
            'address'=>'required',
         ]);


           $input['house_area']=$request->input('house_area');
           $input['price']=$request->input('price');
           $input['room_size']=$request->input('room_size');
           $input['room_number']=$request->input('room_number');
           $input['address']=$request->input('address');
           $input['user_id']=session()->get('user_id');
           $input['status']=1;
           $input['file_name']='';
           $input['file_size']='';
     
           $filename='';
           $filesize=0;

           if($request->hasFile('image'))
           {
             $str = 'advertisement';
             $shuffled = str_shuffle($str);

             $filename=$request->image->getClientOriginalName();
             $filename=$shuffled.$filename;
             $filesize=$request->image->getClientSize();
             $request->image->storeAs('public/advertise',$filename);
           }

           
            $input['file_name']=$filename;
            $input['file_size']=$filesize;


            Advertise::create($input);
            $message="successfully added a new advertisement";
            return redirect('login/create')->with('message',$message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $data=Advertise::findOrFail($id);
        $input['status']=0;
        $data->update($input);
        $message="successfully data deleted as well as it's related cart!";


        $output=Selection::where('advertisement_id', 'LIKE', $id)->get();
         foreach($output as $record){
               $input['status']=0;
               $record->update($input);

             }     

        return redirect()->back()->with('message',$message);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       
    }
}
