<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Users;
use App\Advertise;

class LoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

   public function __construct()
   {
       // $this->middleware('auth', ['only' => ['create', 'store', 'edit', 'delete']]);
       // // Alternativly
       $this->middleware('CheckLogin', ['only' => ['index']]);
       $this->middleware('CheckLogout', ['except' => ['index','store']]);
       $this->middleware('CheckTime', ['only' => ['create']]);
   }

    public function index(Request $request)
    {
        $name=$request->input('name');

        if(!empty($name))
        {
         $alldata=Advertise::where('status','=',1)->where('house_area','LIKE','%'.$name.'%')->paginate(3);
        }              
        else{
         $alldata=Advertise::where('status','=',1)->paginate(3);
        }

        ;
        return view('login',compact('alldata'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $alldata=Advertise::where('status','=',1)->paginate(2);
        
        if(session()->get('user_email')=='admin@co.com'){
                   return view('admin.admin-home',compact('alldata'));
                                                        }
        else{
                     if(session()->get('user_type')=='owner'){
                           return view('owner.owner-home',compact('alldata'));
                                                             }
                     else{
                           return view('renter.renter-home',compact('alldata'));
                         }                                       

             }                                                
    }




    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

         $this->validate($request,[
            'email'=>'required|email|exists:user,email',
            'password'=>'required',
         ]);



         $input['email']=$request->input('email');
         $input['password']=md5($request->input('password'));

 
         $output=Users::where('email',$input['email'])->where('password',$input['password'])->first();  


           if(count($output)==0){
               $message="wrong password !!!";
               return redirect()->back()->with('message',$message);
                                }
           elseif($output['status']==0){
                $message="currently you are not active!";
                return redirect()->back()->with('message',$message);
             }
             else{

                                 session()->put('user_email',$output['email']);
                                 session()->put('user_type',$output['type']);
                                 session()->put('user_id',$output['id']);
                                 session()->put('last_activity',time());
                                 

                $alldata=Advertise::where('status','=',1)->paginate(2); 
                
                if(session()->get('user_email')=='admin@co.com'){
                           return view('admin.admin-home',compact('alldata'));
                                                                }
                else{
                             if(session()->get('user_type')=='owner'){
                                   return view('owner.owner-home',compact('alldata'));
                                                                     }
                             else{
                                   return view('renter.renter-home',compact('alldata'));
                                 }                                       

                     }                                                 

                                 }

    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
