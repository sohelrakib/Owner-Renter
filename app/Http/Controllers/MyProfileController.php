<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Users;

class MyProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data=Users::findOrFail(session()->get('user_id'));
        if(session()->get('user_email')=='admin@co.com'){
                    
                    return view('admin.admin-my-profile-show',compact('data'));
                                                                }
                else{
                             if(session()->get('user_type')=='owner'){
                                   
                                                                     }
                             else{
                                    
                                 }                                       

                     }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data=Users::findOrFail(session()->get('user_id'));
        if(session()->get('user_email')=='admin@co.com'){
                     return view('admin.admin-my-profile-edit',compact('data'));
                                                                }
                else{
                             if(session()->get('user_type')=='owner'){
                                   
                                                                     }
                             else{
                                    
                                 }                                       

                     }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $this->validate($request, [
                      'user_name' => 'required|min:2',
                      'birth'=>'required',
                      'phone'=>'required|min:4',
                      'address'=>'required',
                  ]);

          $data=Users::findOrFail(session()->get('user_id'));

          $input['user_name']=$request->input('user_name');
          $input['birth']=$request->input('birth');
          $input['phone']=$request->input('phone');
          $input['address']=$request->input('address');


           $filename=$data['file_name'];
           $filesize=$data['file_size'];
           
          if($request->hasFile('image'))
          {
            $str = 'abcedvwxyz';
            $shuffled = str_shuffle($str);

            $filename=$request->image->getClientOriginalName();
            $filename=$shuffled.$filename;
            $filesize=$request->image->getClientSize();
            $request->image->storeAs('public/upload',$filename);
          }


          $input['file_name']=$filename;
          $input['file_size']=$filesize;

 
          $data->update($input);
          $message="successfully data updated !!!";
          return redirect('/mypro')->with('message',$message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
