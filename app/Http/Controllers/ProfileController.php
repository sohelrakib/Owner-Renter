<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Users;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        

        if(session()->get('user_email')=='admin@co.com'){
                   $alldata=Users::paginate(3);
                   return view('admin.admin-allmember',compact('alldata'));
                                                        }
        else{
                     if(session()->get('user_type')=='owner'){
                            $alldata=Users::where('status','=',1)->paginate(3);
                            return view('owner.owner-allmember',compact('alldata'));
                                                             }
                     else{
                            $alldata=Users::where('status','=',1)->paginate(3);  
                            return view('renter.renter-allmember',compact('alldata'));
                         }                                       

             }

        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $alldata=Users::findOrFail($id);

        if(session()->get('user_email')=='admin@co.com'){
                    return view('admin.admin-profile-show',compact('alldata'));
                                                        }
        else{
                     if(session()->get('user_type')=='owner'){
                           return view('owner.owner-profile-show',compact('alldata'));
                                                             }
                     else{
                           return view('renter.renter-profile-show',compact('alldata'));
                         }                                       

             }



       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $data=Users::findOrFail($id);

          if($data['status'])
              $input['status']=0;
          else $input['status']=1;

           $data->update($input);
           $message="user's status successfully changed!";
           return redirect()->back()->with('message',$message);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
