<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Users;

class RegistrationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

   return view('registration');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
               'user_name' => 'required|min:2',
               'email'=>'required|email|unique:user,email',
               'birth'=>'required',
               'phone'=>'required|min:4',
               'address'=>'required',
               'password' => 'required|min:2',
               'password_confirm' => 'required|min:2',
           ]);

          
          $input['user_name']=$request->input('user_name');
          $input['email']=$request->input('email');
          $input['gender']=$request->input('gender');
          $input['birth']=$request->input('birth');
          $input['type']=$request->input('type');
          $input['phone']=$request->input('phone');
          $input['password']=md5($request->input('password'));
          $input['password_confirm']=md5($request->input('password_confirm'));
          $input['address']=$request->input('address');
          $input['status']=1;

          $input['file_name']='';
          $input['file_size']='';
           
          $filename='';
          $filesize=0;
       
        if( $input['password']== $input['password_confirm']){
            

              if($request->hasFile('image'))
              {
                $str = 'abcedvwxyz';
                $shuffled = str_shuffle($str);

                $filename=$request->image->getClientOriginalName();
                $filename=$shuffled.$filename;
                $filesize=$request->image->getClientSize();
                $request->image->storeAs('public/upload',$filename);
              }


              echo $input['file_name']=$filename;
              echo $input['file_size']=$filesize;

              Users::create($input);
              $message="you registered successfully! Login now!!";
              return redirect('login')->with('message',$message);

                                                     }
         else{
             $message="your password did not match";
             return redirect()->back()->with('message',$message);
             }                                            

    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
