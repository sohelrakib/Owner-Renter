<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Selection;

class SelectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $alldata=Selection::where('status','=',1)->paginate(2);

         if(session()->get('user_email')=='admin@co.com'){
                    return view('admin.admin-all-select',compact('alldata'));
                                                         }
        

        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
     //my selection: selection by specific person   
        $alldata=Selection::where('users_id','LIKE',session()->get('user_id'))->where('status','=',1)->paginate(2);

         if(session()->get('user_email')=='admin@co.com'){
                    return view('admin.admin-my-selection',compact('alldata'));
                                                         }
         if(session()->get('user_type')=='renter'){
                    return view('renter.renter-my-selection',compact('alldata'));
                                                         }                                               
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $input['advertisement_id']=$id;
        $input['users_id']=session()->get('user_id');
        $input['status']=1;

        $output=Selection::where('users_id',$input['users_id'])->where('advertisement_id',$input['advertisement_id'])->first(); 

        if(count($output)==1)
        {
            $message="you already added it";
            return redirect()->back()->with('message',$message);
        }
        else{
            Selection::create($input);
            $message="successfully added!!!";
            return redirect()->back()->with('message',$message);
        }
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
                  $message="this selection is removed!";
                  $data=Selection::findOrFail($id);
                  $input['status']=0;
                  $data->update($input);
                  return redirect()->back()->with('message',$message);

                  
    }
}
