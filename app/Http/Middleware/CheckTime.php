<?php

namespace App\Http\Middleware;

use Closure;

class CheckTime
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if  (time()-session()->get('last_activity')<6000){
                       return $next($request);   
                                                 }
                 else{
                    session()->flush();
                    return redirect('login');
                 }
    }
}
