<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Selection extends Model
{
   protected $table="selection";

      protected $fillable=[
              'users_id',
              'advertisement_id',
              'status'
                          ];

      public function getUserName()
      {
          return Users::where('id',$this->users_id)->first()->user_name;
      }

      public function getUserEmail()
      {
          return Users::where('id',$this->users_id)->first()->email;
      }

      public function getUserPhone()
      {
          return Users::where('id',$this->users_id)->first()->phone;
      }

      public function getFileName()
      {
          return Advertise::where('id',$this->advertisement_id)->first()->file_name;
      } 

       public function house_area()
      {
          return Advertise::where('id',$this->advertisement_id)->first()->house_area;
      } 

       public function price()
      {
          return Advertise::where('id',$this->advertisement_id)->first()->price;
      }  



      public function getOwnerid()
      {
          return Advertise::where('id',$this->advertisement_id)->first()->user_id;
          
      }

       public function getOwnerName()
      {
          $owner_id= Advertise::where('id',$this->advertisement_id)->first()->user_id;
          return Users::where('id', $owner_id)->first()->user_name;
      }

       public function getOwnerEmail()
      {
          $owner_id= Advertise::where('id',$this->advertisement_id)->first()->user_id;
          return Users::where('id', $owner_id)->first()->email;
      }   

       public function getOwnerPhone()
      {
          $owner_id= Advertise::where('id',$this->advertisement_id)->first()->user_id;
          return Users::where('id', $owner_id)->first()->phone;
      }                 
}
