<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    protected $table="user";

    protected $fillable=[
    	'user_name',
    	'email',
    	'gender',
    	'birth',
    	'type',
    	'phone',
    	'password',
    	'address',
    	'file_name',
    	'file_size',
        'status'
    ];
}
