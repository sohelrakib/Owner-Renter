@extends('admin.admin-master')


   @section('content2')
       <h3 class="text-center"> All Users </h3>

         <p class="messsage text-center" style="color:red"> {{ session('message') }} </p>
               @foreach($alldata as $data)
                       
                            <div class="row">
                              <div class="col-md-4 allprofile">
                                 <img  src="{{ asset('storage/upload/'.$data->file_name)}}" alt="profile image" class=" img-responsive">
                              </div>

                              <div class="col-md-8 ">
                                <div class="table-responsive">          
                                   <table class="table">
                                    <tr>
                                        <th> name :</th>
                                        <td> {{$data->user_name}} </td>
                                    </tr>

                                    <tr>
                                        <th> email :</th>
                                        <td> {{$data->email}} </td>
                                    </tr>

                                    <tr>
                                        <th> contact :</th>
                                        <td> {{$data->phone}} </td>
                                    </tr>

                                    <tr>
                                        <th> type :</th>
                                        <td> {{$data->type}} </td>
                                    </tr>

                                    <tr>
                                        <th> status :</th>
                                        <td> <?php
                                              if($data->status)   
                                                 echo "active";
                                               else echo "deactive";
                                              ?> </td>
                                    </tr>
                                   
                                     <tr>
                                         <th> action :</th>
                                         <td><b> <a href="{{route('profile.edit',$data->id)}} " class="member-action"> <?php
                                              if($data->status)   
                                                 echo "click for deactive";
                                               else echo "click for active";
                                              ?> </a> </b></td>
                                     </tr>

                                  </table>
                              </div>  
                            </div>


                        </div>   
                         <br>
               @endforeach 
                               
                       {!!$alldata->render() !!}
     
   @endsection   