@extends('admin.admin-master')


   @section('content2')

    <h3 class="text-center"> All Deleted Advertisements </h3>

       <p class="messsage text-center" style="color:red"> {{ session('message') }} </p>
                     
                     <div class="table-responsive">
                       <table class="table table-striped table-condensed table-bordered">
                       	 <thead>
                       	    <tr>
	                       	 	<th> owner </th>
	                       	 	<th> house area </th>
	                       	 	<th> address </th>
	                       	 	<th> room size </th>
	                       	 	<th> price </th>
	                       	 	<th> image </th>	
                            </tr>         
                         </thead>                      
                           @foreach($alldata as $data)
                         <tbody>
                             <tr>
	                            <td><a href="{{route('profile.show',$data->user_id)}}"> {{$data->getUserEmail()}} </a></td> 
	                            <td> {{$data->house_area}} </td>
	                            <td> {{$data->address}} </td>
	                            <td> {{$data->room_size}} sf </td>
	                            <td> {{$data->price}} bdt </td>
	                            <td> <img class="img-responsive" src="{{ asset('storage/advertise/'.$data->file_name)}}" alt="product"> </td>
                             </tr>
                         </tbody>  
                           @endforeach 
                       </table>  
                      </div>     
                                   {!!$alldata->render() !!}
                   
   @endsection   