@extends('admin.admin-master')


   @section('content2')

    <h3 class="text-center"> All Advertisements </h3>

       <p class="messsage text-center" style="color:red"> {{ session('message') }} </p>


                                   
                                                 
                           @foreach($alldata as $data)
                                   
                                        <div class="row">
                                          <div class="col-md-6 img-content">
                                             <img class="advertise-img" src="{{ asset('storage/advertise/'.$data->file_name)}}" alt="product" class="img-responsive">
                                                 <div class="overlay">

                                                  <a href="{{route('select.show',$data->id)}}">
                                                    <div class="text">
                                                       add to cart 
                                                    </div>
                                                    </a>
                                                  </div>   
                                          </div>
                                          <div class="col-md-6 advertise-content">
                                            <div class="table-responsive">          
                                               <table class="table">
                                              <tr>
                                                 <th> owner </th>
                                                 <td> <a href="{{route('profile.show',$data->user_id)}}"> {{$data->getUserEmail()}} </a> </td>
                                              </tr>
                                                <tr>
                                                 <th> phone </th>
                                                 <td> {{$data->getUserPhone()}} </td>
                                              </tr>
                                               <tr>
                                                 <th> house area: </th>
                                                 <td> {{$data->house_area}} </td>
                                              </tr>
                                               <tr>
                                                 <th> price: </th>
                                                 <td> {{$data->price}} bdt </td>
                                              </tr>
                                               <tr>
                                                 <th> room size: </th>
                                                 <td> {{$data->room_size}} sf </td>
                                              </tr>
                                              <tr>
                                                 <th> room number: </th>
                                                 <td> {{$data->room_number}} </td>
                                              </tr>
                                              <tr>
                                                 <th> address </th>
                                                 <td> {{$data->address}} </td>
                                              </tr>
                                              <tr>
                                                 <th> comment </th>
                                                 <td> 
                                                    <a href="{{route('advcomment.show',$data->id)}}"><button type="button" class="btn btn-primary"> click here </button></a>
                                                  </td>
                                              </tr>

                                              </table>
                                          </div>  
                                        </div>


                                    </div>   
                          
                           @endforeach 
                                           
                                   {!!$alldata->render() !!}
                   
   @endsection   