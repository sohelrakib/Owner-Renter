@extends('master')

   @section('title')
        ***Admin Page
   @endsection


   @section('content')
                   <div class="row">
                   	 <h1 class="text-center"> Admin Panel </h1>
                     <div class="col-md-9 right-side">
                     	  @yield('content2')
                     </div>

                     <div class="col-md-3 ">
                           <ul class="sidebar-ul">
                                <li> <a href="{{route('login.create')}}"><button type="button" class="btn btn-primary"> home page </button></a> </li>

                                <li> <a href="{{route('advertise.index')}}"><button type="button" class="btn btn-primary"> new advertise </button></a> </li>

                                <li> <a href="{{route('profile.index')}}"><button type="button" class="btn btn-primary"> all member </button></a> </li>

                                <li> <a href="{{route('select.index')}}"><button type="button" class="btn btn-primary"> all selection </button></a> </li>

                                <li> <a href="{{route('select.create')}}"><button type="button" class="btn btn-primary"> my selection </button></a> </li>

                                <li> <a href="{{route('advertise.create')}}"><button type="button" class="btn btn-primary"> my advertise </button></a> </li>

                                <li> <a href="{{route('delete.index')}}"><button type="button" class="btn btn-primary"> deleted advertise </button></a> </li>

                                <li> <a href="{{route('mypro.index')}}"><button type="button" class="btn btn-primary"> my profile </button></a> </li>


                                <li> <a href="{{route('statistic.index')}}"><button type="button" class="btn btn-primary"> statistic </button></a> </li>

                                <li> <a href="{{route('logout.index')}}"><button type="button" class="btn btn-primary"> logout </button></a> </li>



                                
                           
                           </ul>
                     </div>
                                 
                   
                    </div>     
   @endsection   