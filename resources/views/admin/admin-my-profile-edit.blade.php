@extends('admin.admin-master')


   @section('content2')
       <h3 class="text-center"> Edit Your Info </h3>


    <div class="row">
       <div class="col-md-8 col-md-offset-2">

         <p class="messsage text-center" style="color:red"> {{ session('message') }} </p>
      
           @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                 </div>
            @endif


            {!! Form::open(['route' => ['mypro.update', $data->id],'method' => 'put','files' => true]) !!}
                                               <div class="form-group row">
                                                       <div class="col-md-3 col-sm-3 col-xs-3">
                                                           <label class="contrl-label" for="user_name"> Name:</label>
                                                       </div>
                                                       <div class="col-md-9 col-sm-9 col-xs-9">
                                                           <input type="text" class="form-control" name="user_name"  value="{{ ($data->user_name) }}" required>
                                                       </div>
                                               </div>

                                                <div class="form-group row">
                                                       <div class="col-md-3 col-sm-3 col-xs-3">
                                                           <label class="control-label" for="email">Email: </label>
                                                       </div>
                                                       <div class="col-md-9 col-sm-9 col-xs-9">
                                                            <input type="email" class="form-control" name="email"  value="{{ ($data->email) }}" readonly> 
                                                       </div>
                                                </div>


                                                <div class="form-group row">
                                                       <div class="col-md-3 col-sm-3 col-xs-3">
                                                           <label class="control-label" for="phone">phone: </label>
                                                       </div>
                                                       <div class="col-md-9 col-sm-9 col-xs-9">
                                                            <input type="text" class="form-control" name="phone"  value="{{ ($data->phone) }}" required> 
                                                       </div>
                                                </div>


                                                <div class="form-group row">
                                                       <div class="col-md-3 col-sm-3 col-xs-3">
                                                           <label class="control-label" for="address">address: </label>
                                                       </div>
                                                       <div class="col-md-9 col-sm-9 col-xs-9">
                                                            <input type="text" class="form-control" name="address"  value="{{ ($data->address) }}" required> 
                                                       </div>
                                                </div>

                                                <div class="form-group row">
                                                       <div class="col-md-3 col-sm-3 col-xs-3">
                                                           <label class="control-label" for="birth">birth: </label>
                                                       </div>
                                                       <div class="col-md-9 col-sm-9 col-xs-9">
                                                            <input type="date" class="form-control" name="birth"  value="{{ ($data->birth) }}"> 
                                                       </div>
                                                </div>

                                                 <div class="form-group row">
                                                       <div class="col-md-3 col-sm-3 col-xs-3">
                                                            <label class="control-label" for="image">Image: </label>
                                                        </div>
                                                        <div class="col-md-9 col-sm-9 col-xs-9">
                                                             <input type="file" class="form-control" name="image" id="image"> 
                                                        </div>
                                                  </div>

                                                <input class="btn btn-success" type="submit" value="Submit">
                                       {!! Form::close() !!}

         </div>
       </div>   
                       
                            
     
   @endsection   