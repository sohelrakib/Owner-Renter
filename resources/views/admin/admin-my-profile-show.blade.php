@extends('admin.admin-master')


   @section('content2')
       <h3 class="text-center"> My Info </h3>

         <p class="messsage text-center" style="color:red"> {{ session('message') }} </p>


          <div class="row">
          	<div class="col-md-6 col-md-offset-3">
              <div class="card own-profile">
                 <img  src="{{ asset('storage/upload/'.$data->file_name)}}" alt="profile image" class=" img-responsive">

                 <h4><b> {{$data->user_name}} </b></h4>
                 <p class="own-email">  {{$data->email}}     </p> 
                 <p>  {{$data->phone}}     </p>
                 <p>  type: {{$data->type}}     </p>
                 <p>  address: {{$data->address}}     </p>
                 <p>  birth: {{$data->birth}}     </p>
                 <p>  gender: {{$data->gender}}     </p>

                 <a href="{{route('mypro.edit',$data->id)}}" class="edit-btn"><button type="button" class="btn btn-primary"> click for edit </button></a>
              </div>
             </div> 
          </div>
                       
                            
     
   @endsection   