@extends('admin.admin-master')


   @section('content2')
    <h3 class="text-center"> My Selection </h3>
                     
     <p class="messsage text-center" style="color:red"> {{ session('message') }} </p>

              
                              
        @foreach($alldata as $data)
                
                     <div class="row">
                       <div class="col-md-6 img-content">
                             <img class="advertise-img" src="{{ asset('storage/advertise/'.$data->getFileName())}}" alt="product" class="img-responsive">
                                   <div class="overlay">

                                    
                                      <div class="text">
                            {!! Form::open(['route' => ['select.destroy', $data->id],'method' => 'delete']) !!}
                            {!!Form::hidden('id',$data->id)!!}
                            {!!Form::submit('click for remove',array('class'=>'btn btn-danger','onclick'=>'return myFunction()'))!!}
                            {!! Form::close() !!}
                                      </div>
                                      
                                    </div>  
                       </div>
                       <div class="col-md-6 advertise-content">
                         <div class="table-responsive">          
                            <table class="table">

                             <tr>
                               <th> user name: </th>
                               <td> {{$data->getUserName()}} </td>
                             </tr>

                             <tr>
                               <th> user email: </th>
                               <td> <a href="{{route('profile.show',$data->users_id)}}">{{$data->getUserEmail()}} </a></td>
                             </tr>

                             <tr>
                               <th> user phone: </th>
                               <td> {{$data->getUserPhone()}} </td>
                             </tr>
                            
                              <tr>
                                <th> house area: </th>
                                <td> {{$data->house_area()}} </td>
                              </tr>

                              <tr>
                                <th> price: </th>
                                <td> {{$data->price()}} </td>
                              </tr>

                              <tr>
                                <th> owner name: </th>
                                <td> {{$data->getOwnerName()}} </td>
                              </tr>

                              <tr>
                                <th> owner email: </th>
                                <td> <a href="{{route('profile.show',$data->getOwnerid())}}">{{$data->getOwnerEmail()}} </a></td>
                              </tr>

                               <tr>
                                <th> owner phone: </th>
                                <td> {{$data->getOwnerPhone()}} </td>
                              </tr> 

                               

                           </table>
                       </div>  
                     </div>


                 </div>   
        
        @endforeach 
                        
                {!!$alldata->render() !!}

         <?php  if(count($alldata)==0)  { ?>
             <h3 class="text-center no-data"> no data </h3>

         <?php  }  ?>  
   @endsection   