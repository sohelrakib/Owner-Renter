@extends('master')

   @section('title')
        welcome to login here
   @endsection


   @section('content')
                   <div class="row">
     
                        <div class="col-md-5 text-center go-registration">
                           <h5 ><b>don't have an account!!</b></h5>
                           <h5 ><b>click below for registration.</b></h5>

                            <a  href="{{ route('registration.index')}}"> go to registration </a>
                        </div>
                       
                         <div class="col-md-6 col-md-offset-1">
                                <h4 class="first-heading text-center"><b>Welcome to Login Here: </b></h4>
                                  @if ($errors->any())
                                       <div class="alert alert-danger">
                                           <ul>
                                               @foreach ($errors->all() as $error)
                                                   <li>{{ $error }}</li>
                                               @endforeach
                                           </ul>
                                       </div>
                                   @endif
                               
                       <p class="messsage text-center" style="color:red"> {{ session('message') }} </p>

                            
                             {!! Form::open(array('route'=>'login.store')) !!}
                                <div class="w3-card login-card">
                                      <div class="form-group row">
                                             <div class="col-md-3 col-sm-3 col-xs-3">
                                                 <label class="contrl-label" for="email">Email:</label>
                                             </div>
                                             <div class="col-md-9 col-sm-9 col-xs-9">
                                                  <input type="email" class="form-control" name="email"  value="{{ old('email') }}" required>
                                              </div>
                                       </div>

                                    

                                       <div class="form-group row">
                                              <div class="col-md-3 col-sm-3 col-xs-3">
                                                   <label class="control-label" for="password">Password: </label>
                                               </div>
                                               <div class="col-md-9 col-sm-9 col-xs-9">
                                                    <input type="password" class="form-control" name="password"  value="{{ old('password') }}" required> 
                                               </div>
                                       </div>

                                     <!--  ,'onclick'=>'return myFunction1()' -->
                                       {!!Form::submit('submit',array('class'=>'btn btn-success'))!!}
                               </div>
                              {!! Form::close() !!}

                           </div>

                          

                           <div class="col-md-10">
                            <br><br>
                      <h3 class="text-center"> Search Your Prefered House ! </h3>
                    

                             <div class="search">
                        {!! Form::open(['route'=>'login.index','method'=>'GET','class'=>'search-form']) !!}
                               <div class="form-group row">
                                   <div class="col-md-9 col-sm-9 col-xs-9">
                                        <input type="text" name="name"  class="form-control col-md-10" placeholder="  search by area name">
                                    </div>
                                    <div class="col-md-3 col-sm-3 col-xs-3">
                        {!!Form::submit('search',array('class'=>'search-button btn btn-default')) !!}
                                    </div>
                               </div>
                        {!! Form:: close() !!}
                             </div> <!--  search -->


                               <div class="table-responsive">
                       <table class="table table-striped table-condensed table-bordered">
                         <thead>
                            <tr>
                            <th> house area </th>
                            <th> room size </th>
                            <th> price </th>
                            <th> image </th>  
                            </tr>         
                         </thead>                      
                           @foreach($alldata as $data)
                         <tbody>
                             <tr> 
                              <td> {{$data->house_area}} </td>
                              <td> {{$data->room_size}} sf </td>
                              <td> {{$data->price}} bdt </td>
                              <td> <img class="img-responsive front-image" src="{{ asset('storage/advertise/'.$data->file_name)}}" alt="product"> </td>
                             </tr>
                         </tbody>  
                           @endforeach 
                       </table>  
                      </div>     
                                   {!!$alldata->render() !!}
                           </div>
                   
                        </div>     
   @endsection   