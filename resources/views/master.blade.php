<html>
     <head>
          <title> @yield('title') </title>
          <meta charset="utf-8">
          <meta name="viewport" content="width=device-width, initial-scale=1">
          <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
         
          <link rel="stylesheet" type="text/css" href="{{ URL::to('first.css')}}">
          <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
          <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
     </head>


     <body>
          <header>
                <div class="container">
                     <div class="row">
                         <div class="col-md-12">
                                  <nav class="navbar navbar-inverse">
                                         <div class="row">
                                            <div class="navbar-header">
                                              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                                                      <span class="icon-bar"></span>
                                                      <span class="icon-bar"></span>
                                                      <span class="icon-bar"></span> 
                                                </button>
                                               </div>
                                                 <div class="col-md-6 col-md-offset-4">
                                                     <div class="collapse navbar-collapse" id="myNavbar">
                                                         <ul class="nav navbar-nav">
                                                           <li><a href="#">LOGO</a></li>
                                                           <li><a href="#">HOME</a></li>
                                                           <li><a href="#">ABOUT</a></li>
                                                           <li><a href="#">SERVICE</a></li>
                                                           <li><a href="#">CONTACT</a></li>
                                                         </ul>
                                                     </div>
                                                 </div>
                                           </div>
                                    
                                   </nav>
                          </div>
                     </div>
                </div>
          </header>



          <div class="container">
              <div class="row">
                  <div class="col-md-3">
                      <div class="al-content">
                           <ul class="sidebar-ul">

                                <li> <a href="{{route('login.index')}}"><button type="button" class="btn btn-primary"> Login </button></a> </li>

                                <li> <a href="{{route('registration.index')}}"><button type="button" class="btn btn-primary"> Registration </button></a> </li>

                                <li> <a href="{{route('sentmail.index')}}"><button type="button" class="btn btn-primary"> mail </button></a> </li>

                                
                           
                           </ul>
                      </div>
                  </div>
                  <div class="col-md-9">
                      @yield('content')
                  </div>
              </div>
                 
          </div>



             <footer>

             </footer>
            
             <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
             <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

             <script>
               function myFunction() {
                   if(!confirm("Are You Sure to delete this"))
                   event.preventDefault();
               }
             </script>
     </body>

</html>