@extends('owner.owner-master')


   @section('content2')
    <h4 class="text-center"> Make an Advertisement </h4>
                    @if ($errors->any())
                         <div class="alert alert-danger">
                             <ul>
                                 @foreach ($errors->all() as $error)
                                     <li>{{ $error }}</li>
                                 @endforeach
                             </ul>
                         </div>
                     @endif
                 
     <p class="messsage text-center" style="color:red"> {{ session('message') }} </p>

              
               {!! Form::open(array('route'=>'advertise.store','files' => true)) !!}
                     <div class="w3-card login-card">
                        <div class="form-group row">
                               <div class="col-md-3 col-sm-3 col-xs-3">
                                   <label class="contrl-label" for="house_area">House Area:</label>
                               </div>
                               <div class="col-md-9 col-sm-9 col-xs-9">
                                    <input type="text" class="form-control" name="house_area"   value="{{ old('house_area') }}" >
                                </div>
                         </div>

                      

                         <div class="form-group row">
                                <div class="col-md-3 col-sm-3 col-xs-3">
                                     <label class="control-label" for="price">Price(bdt): </label>
                                 </div>
                                 <div class="col-md-9 col-sm-9 col-xs-9">
                                      <input type="number" class="form-control" name="price" value="{{ old('price') }}" > 
                                 </div>
                         </div>



                         <div class="form-group row">
                                <div class="col-md-3 col-sm-3 col-xs-3">
                                     <label class="control-label" for="room_size">Room Size(sf): </label>
                                 </div>
                                 <div class="col-md-9 col-sm-9 col-xs-9">
                                      <input type="number" class="form-control" name="room_size" value="{{ old('room_size') }}" > 
                                 </div>
                         </div>
                       
                        

                      <div class="form-group row">
                               <div class="col-md-3 col-sm-3 col-xs-3">
                                    <label class="control-label" for="room_number">Room Number: </label>
                                </div>
                                <div class="col-md-9 col-sm-9 col-xs-9">
                                     <input type="number" class="form-control" name="room_number" value="{{ old('room_number') }}" > 
                                </div>
                        </div>



                         <div class="form-group row">
                               <div class="col-md-3 col-sm-3 col-xs-3">
                                    <label class="control-label" for="address">Address: </label>
                                </div>
                                <div class="col-md-9 col-sm-9 col-xs-9">
                                     <input type="text" class="form-control" name="address" value="{{ old('address') }}"> 
                                </div>
                         </div>


                         <div class="form-group row">
                               <div class="col-md-3 col-sm-3 col-xs-3">
                                    <label class="control-label" for="image">Image: </label>
                                </div>
                                <div class="col-md-9 col-sm-9 col-xs-9">
                                     <input type="file" class="form-control" name="image" id="image"> 
                                </div>
                        </div>

                       
                      
                       <!--  ,'onclick'=>'return myFunction1()' -->
                         {!!Form::submit('submit',array('class'=>'btn btn-success'))!!}
                       </div>
                {!! Form::close() !!}


   @endsection   