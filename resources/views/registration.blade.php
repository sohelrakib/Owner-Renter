@extends('master')

   @section('title')
        welcome to registration here
   @endsection


   @section('content')
                   <div class="row">
                      <div class="col-md-9 col-md-offset-2">
                       
                       
                         
                                <h3 class="first-heading text-center"><b>Welcome to Registration Here: </b></h3>

                                       

                                                  @if ($errors->any())
                                                       <div class="alert alert-danger">
                                                           <ul>
                                                               @foreach ($errors->all() as $error)
                                                                   <li>{{ $error }}</li>
                                                               @endforeach
                                                           </ul>
                                                       </div>
                                                   @endif
                                               
                                   <p class="messsage text-center" style="color:red"> {{ session('message') }} </p>

                                            
                                             {!! Form::open(array('route'=>'registration.store','files' => true)) !!}
                                              <div class="w3-card login-card">
                                                      <div class="form-group row">
                                                             <div class="col-md-3 col-sm-3 col-xs-3">
                                                                 <label class="contrl-label" for="user_name">Name:</label>
                                                             </div>
                                                             <div class="col-md-9 col-sm-9 col-xs-9">
                                                                  <input type="text" class="form-control" name="user_name"   value="{{ old('user_name') }}" >
                                                              </div>
                                                       </div>

                                                    

                                                       <div class="form-group row">
                                                              <div class="col-md-3 col-sm-3 col-xs-3">
                                                                   <label class="control-label" for="email">Email: </label>
                                                               </div>
                                                               <div class="col-md-9 col-sm-9 col-xs-9">
                                                                    <input type="email" class="form-control" name="email" value="{{ old('email') }}" > 
                                                               </div>
                                                       </div>



                                                       <div class="form-group row">
                                                             <div class="col-md-3 col-sm-3 col-xs-3">
                                                                 <label class="contrl-label" for="gender">Gender:</label>
                                                             </div>
                                                             <div class="col-md-9 col-sm-9 col-xs-9">
                                                                  <input type="radio" name="gender" value="male" checked="checked"> Male
                                                                  <input type="radio" name="gender" value="female"> Female
                                                                  
                                                              </div>
                                                       </div>


                                                     

                                                       <div class="form-group row">
                                                              <div class="col-md-3 col-sm-3 col-xs-3">
                                                                   <label class="control-label" for="birth">Birth Date: </label>
                                                               </div>
                                                               <div class="col-md-9 col-sm-9 col-xs-9">
                                                                    <input type="date" class="form-control" name="birth"  > 
                                                               </div>
                                                       </div>


                                                       <div class="form-group row">
                                                             <div class="col-md-3 col-sm-3 col-xs-3">
                                                                 <label class="contrl-label" for="type">Type:</label>
                                                             </div>
                                                             <div class="col-md-9 col-sm-9 col-xs-9">
                                                                  <input type="radio" name="type" value="renter" checked="checked"> Renter
                                                                  <input type="radio" name="type" value="owner"> Owner
                                                                  
                                                              </div>
                                                       </div>
                                                     
                                                      

                                                    <div class="form-group row">
                                                             <div class="col-md-3 col-sm-3 col-xs-3">
                                                                  <label class="control-label" for="phone">Phone: </label>
                                                              </div>
                                                              <div class="col-md-9 col-sm-9 col-xs-9">
                                                                   <input type="text" class="form-control" name="phone" value="{{ old('phone') }}" > 
                                                              </div>
                                                      </div>




                                                       <div class="form-group row">
                                                              <div class="col-md-3 col-sm-3 col-xs-3">
                                                                   <label class="control-label" for="password">Password: </label>
                                                               </div>
                                                               <div class="col-md-9 col-sm-9 col-xs-9">
                                                                    <input type="password" class="form-control" name="password"  value="{{ old('password') }}" > 
                                                               </div>
                                                       </div>


                                                       <div class="form-group row">
                                                              <div class="col-md-3 col-sm-3 col-xs-3">
                                                                   <label class="control-label" for="password_confirm">Password(again): </label>
                                                               </div>
                                                               <div class="col-md-9 col-sm-9 col-xs-9">
                                                                    <input type="password" class="form-control" name="password_confirm"  value="{{ old('password_confirm') }}" > 
                                                               </div>
                                                       </div>


                                                       <div class="form-group row">
                                                             <div class="col-md-3 col-sm-3 col-xs-3">
                                                                  <label class="control-label" for="address">Address: </label>
                                                              </div>
                                                              <div class="col-md-9 col-sm-9 col-xs-9">
                                                                   <input type="text" class="form-control" name="address" value="{{ old('address') }}"> 
                                                              </div>
                                                       </div>


                                                       <div class="form-group row">
                                                             <div class="col-md-3 col-sm-3 col-xs-3">
                                                                  <label class="control-label" for="image">Image: </label>
                                                              </div>
                                                              <div class="col-md-9 col-sm-9 col-xs-9">
                                                                   <input type="file" class="form-control" name="image" id="image"> 
                                                              </div>
                                                      </div>

                                                     <!--  ,'onclick'=>'return myFunction1()' -->
                                                       {!!Form::submit('submit',array('class'=>'btn btn-success'))!!}
                                                 </div>
                                              {!! Form::close() !!}

                                                 
                         </div>
                       </div>

   @endsection   