@extends('renter.renter-master')


   @section('content2')
      
               @foreach($alldata as $data)
                       
                            <div class="row">
                              <div class="col-md-4 allprofile">
                                 <img  src="{{ asset('storage/upload/'.$data->file_name)}}" alt="profile image" class=" img-responsive">
                              </div>

                              <div class="col-md-8 ">
                                <div class="table-responsive">          
                                   <table class="table">
                                    <tr>
                                        <th> name :</th>
                                        <td> {{$data->user_name}} </td>
                                    </tr>

                                    <tr>
                                        <th> email :</th>
                                        <td> {{$data->email}} </td>
                                    </tr>

                                    <tr>
                                        <th> contact :</th>
                                        <td> {{$data->phone}} </td>
                                    </tr>

                                    <tr>
                                        <th> type :</th>
                                        <td> {{$data->type}} </td>
                                    </tr>
                                  

                                  </table>
                              </div>  
                            </div>


                        </div>   
                         <br>
               @endforeach 
                               
                       {!!$alldata->render() !!}
     
   @endsection   