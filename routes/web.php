<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::resource('login','LoginController');
Route::resource('registration','RegistrationController', ['middleware' => 'CheckLogin']);



Route::group(['middleware' => 'CheckTime'], function()
{
    Route::group(['middleware' => 'CheckLogout'], function()
      {
			Route::resource('advertise','AdvertiseController');
			Route::resource('advcomment','AdvertiseCommentController');
			Route::resource('profile','ProfileController');
			Route::resource('select','SelectController');
			Route::resource('logout','LogoutController');
			Route::resource('delete','DeletedController');
			Route::resource('mypro','MyProfileController');
			Route::resource('statistic','StatisticsController');
      });

});



Route::resource('sentmail','MailController');